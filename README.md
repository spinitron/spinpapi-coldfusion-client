Coldfusion SpinPapi sample request
=====

sample_request.cfm is demo code. It illunstrates an approach to composition and signing of a 
SpinPapi request in Coldusion markup. Documentation is in the code comments.

### Author, copyright and license

sample_request.cfm was kindly provided by Matt Schaefer of 
[Airbubble Industries](http://www.airbubbleindustries.com/),
while working for [KDNK Community Radio](http://www.kdnk.org/) Carbondale, CO., who 
commits it to the public domain.

A demarketed segment at the beginning of sample_request.cfm was taken from 
[Ben Nadel's website](http://www.bennadel.com/blog/2656-url-encoding-amazon-s3-resource-keys-for-pre-signed-urls-in-coldfusion.htm).
Ben Nadel's website has no copyright notice or license for the content so we assume that
the author commits the code fragments on the referenced page to the public domain. Please
let us know if this assumption is incorrect.


#### Public domain statement and disclaimers

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

