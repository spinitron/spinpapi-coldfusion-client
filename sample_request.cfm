﻿<!--- take from Ben Nadel's awesomeness :
	http://www.bennadel.com/blog/2656-url-encoding-amazon-s3-resource-keys-for-pre-signed-urls-in-coldfusion.htm
	this is so we can comply with Spinitron's req. to use URL Encoding with rfc 3986
--->

<cfscript>
  public string function urlEncodeS3Key( required string key ) {
          key = urlEncodedFormat( key, "utf-8" );
          // At this point, we have a key that has been encoded too aggressively by
          // ColdFusion. Now, we have to go through and un-escape the characters that
          // AWS does not expect to be encoded.
          // The following are "unreserved" characters in the RFC 3986 spec for Uniform
          // Resource Identifiers (URIs) - http://tools.ietf.org/html/rfc3986#section-2.3
          key = replace( key, "%2E", ".", "all" );
          key = replace( key, "%2D", "-", "all" );
          key = replace( key, "%5F", "_", "all" );
          key = replace( key, "%7E", "~", "all" );
          // Technically, the "/" characters can be encoded and will work. However, if the
          // bucket name is included in this key, then it will break (since it will bleed
          // into the S3 domain: "s3.amazonaws.com%2fbucket"). As such, I like to unescape
          // the slashes to make the function more flexible. Plus, I think we can all agree
          // that regular slashes make the URLs look nicer.
          key = replace( key, "%2F", "/", "all" );
          // This one isn't necessary; but, I think it makes for a more attactive URL.
          // --
          // NOTE: That said, it looks like Amazon S3 may always interpret a "+" as a
          // space, which may not be the way other servers work. As such, we are leaving
          // the "+"" literal as the encoded hex value, %2B.
          key = replace( key, "%20", "+", "all" );
          return( key );
      }
</cfscript>

<!--- end of Ben Nadel's awesomeness --->

<cfset variables.userid = "your_spinpapi_userid">
<cfset variables.secret = "your_spinpapi_secret">
<cfset variables.apiUri = "http://spinitron.com/public/spinpapi.php?">

<!--- Get the current GMT time. --->
<cfset timestampGMT = DateConvert( "Local2UTC", now() ) />
<cfset timestampGMT = dateFormat(timestampGMT, "Y-mm-dd") & "T" & timeFormat(timestampGMT, "HH:mm:ss") & "Z">

<!--- say what you will about this silliness below, but I just needed a quick way to have an
orderable struct for my demo, so I used a query object. feel free to use something more succinct and pretty  --->
<cfset apiParams = queryNew("name,value")>
<cfset temp = QueryAddRow(apiParams)>
<cfset Temp = QuerySetCell(apiParams, "name", "timestamp")>
<cfset Temp = QuerySetCell(apiParams, "value", timestampGMT)>
<cfset temp = QueryAddRow(apiParams)>
<cfset Temp = QuerySetCell(apiParams, "name", "method")>
<cfset Temp = QuerySetCell(apiParams, "value", "getSongs")>
<cfset temp = QueryAddRow(apiParams)>
<cfset Temp = QuerySetCell(apiParams, "name", "station")>
<cfset Temp = QuerySetCell(apiParams, "value", "wkrp")>
<cfset temp = QueryAddRow(apiParams)>
<cfset Temp = QuerySetCell(apiParams, "name", "papiversion")>
<cfset Temp = QuerySetCell(apiParams, "value", "2")>
<cfset temp = QueryAddRow(apiParams)>
<cfset Temp = QuerySetCell(apiParams, "name", "papiuser")>
<cfset Temp = QuerySetCell(apiParams, "value", variables.userid)>

<!--- reordered the value pairs to comply with the spinpApi req to have the param in alpha order --->
<cfquery dbtype="query" name="apiParams">
  SELECT *
  FROM apiParams
  ORDER BY name ASC
</cfquery>

<cfset variables.query = "">

<!--- loop over the params and encode the values... technically you need to encode the name too (for spaces i guess?) --->
<cfoutput query="apiParams">
  <cfset variables.thiskey = urlEncodeS3Key(apiParams.name) & "=" &  urlEncodeS3Key(apiParams.value)>
  #variables.thiskey#   <br>
  <cfset variables.query = listAppend(variables.query,variables.thiskey,"&")>
</cfoutput>

<cfset variables.subject = "spinitron.com\n/public/spinpapi.php\n">
<cfset variables.subject = replace( variables.subject, "\n", chr( 10 ), "all" ) />
<cfset variables.subject = variables.subject & variables.query>

<cfset variables.signature =  hmac( variables.subject, variables.secret, "HmacSHA256" )>
<cfset variables.signature = binaryEncode( binaryDecode(variables.signature, "hex"), "base64" )>


<cfset variables.signature = "signature=" & urlEncodeS3Key(variables.signature)>

<cfset variables.query = variables.query & "&" & variables.signature>
<cfset variables.request = variables.apiUri & variables.query>

<cfhttp url="#variables.request#" method="GET" throwOnError = "yes" timeout = "100" />

<cfdump var="#cfhttp.FileContent#">
